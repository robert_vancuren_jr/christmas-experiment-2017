/* jshint node: true */
var babel = require("rollup-plugin-babel");

export default [
    {
        name: "fireplace",
        extend: "true",
        input: "src/index.js",
        plugins: rollupPlugins(),
        output: {
            format: "umd",
            file: "dist/fireplace.js"
        }
    },
];

function rollupPlugins() {
    return [
        babel({
            presets: [["env", {modules: false}]],
            plugins: ["external-helpers"]
        })
    ];
}
